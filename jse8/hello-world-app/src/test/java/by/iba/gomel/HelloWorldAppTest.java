package by.iba.gomel;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

public class HelloWorldAppTest {

    @Rule
    public final SystemOutRule log = new SystemOutRule().enableLog();

    @Test
    public void testMain() {
        HelloWorldApp.main(new String[] {});
        Assert.assertEquals("Hello World! string should be in system out", "Hello World!",
                this.log.getLog());
    }
    
    @Test
    public void getAppName() {
    	Assert.assertEquals("Hello World App", HelloWorldApp.getAppName());
    }

}
